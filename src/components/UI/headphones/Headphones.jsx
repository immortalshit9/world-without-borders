import React from 'react';
import { useState } from 'react';
import headphones from './../../../assets/images/headphones.svg';
import headphonesHover from './../../../assets/images/headphones-hover.svg';

const Headphones = () => {
    const [isHover, setIsHover] = useState(false);
    return (
        <div className='headphonesWrap'
            style={{cursor:"pointer"}}
            onMouseEnter={() => setIsHover(true)}
            onMouseLeave={() => setIsHover(false)}
        >
            {!isHover && (<img
                src={headphones}
                className='headphones'
                alt='headphones'
            />)}
            {isHover && (<img
                src={headphonesHover}
                className='headphonesHover'
                alt='headphones'
            />)}
        </div>
    );
};

export default Headphones;
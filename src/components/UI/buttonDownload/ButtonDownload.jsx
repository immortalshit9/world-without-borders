import React from 'react';
import { useState } from 'react';
import arrowForButton from './../../../assets/images/arrow-for-button.svg';

const ButtonDownload = ({ children }) => {
    const [isHover, setIsHover] = useState(false);
    return (
        <button
            className='download-buttons__btn'
            type='button'
            onMouseEnter={() => setIsHover(true)}
            onMouseLeave={() => setIsHover(false)}
        >
            {!isHover && (
                <div className='download-buttons__description'>
                    {children}
                </div>
            )}
            {isHover && (
                <>
                    <div className='download-buttons__description'
                        style={{ marginLeft: '23px' }}
                    >
                        {children}
                    </div>
                    <img className='download-buttons__arrow'
                        src={arrowForButton}
                        alt='arrow'
                        style={{marginRight: '30px'}}
                    />
                </>
            )}

        </button>
    );
};

export default ButtonDownload;
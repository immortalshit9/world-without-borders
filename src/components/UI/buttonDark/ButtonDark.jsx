import React from 'react';
import classes from './buttonDark.module.css';

const buttonDark = ({children}) => {
    return (
        <button type="button" className={classes.btnDark}>{children}</button>
    );
};

export default buttonDark;
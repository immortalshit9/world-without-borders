import React from 'react';
import hearth from './../../../assets/images/hearth.svg';
import hearthHover from './../../../assets/images/hearth-hover.svg';
import hearthClicked from './../../../assets/images/hearth-clicked.svg';
import { useState } from 'react';

const Hearth = () => {
    const [isHover, setIsHover] = useState(false);
    return (
        <div className='hearthWrap'
            style={{ cursor: "pointer" }}
            onMouseEnter={() => setIsHover(true)}
            onMouseLeave={() => setIsHover(false)}
        >
            {!isHover && (
                <img src={hearth}
                    alt='hearth'
                    className='hearth'
                />
            )}
            {isHover && (
                <img src={hearthHover}
                    className='hearth'
                    alt='hearthHover'
                />
            )}
        </div>
    );
};

export default Hearth;
import React from 'react';
import arrowMore from './../../../assets/images/arrow-more.svg';
import { useState } from 'react';

const ButtonDoMore = (props) => {
    const [isHover, setIsHover] = useState(false)

    return (
        <div className='button-more-wrap'
            style={{ cursor: 'pointer' }}
            onMouseEnter={() => setIsHover(true)}
            onMouseLeave={() => setIsHover(false)}
        >
            {!isHover && (
                <>
                    <span className='button-more__text'
                        style={{ marginLeft: props.margL }}>
                        {props.todo}
                    </span>
                    <img
                        src={arrowMore}
                        alt='arrow'
                        className='button-more__arrow'
                    />
                </>
            )}
            {isHover && (
                <>
                    <span className='button-more__text'
                        style={{
                            marginLeft: props.margL,
                            marginRight: '26.6px'
                        }}>
                        {props.todo}
                    </span>
                    <img
                        src={arrowMore}
                        alt='arrow'
                        className='button-more__arrow'
                    />
                </>
            )}
        </div>
    );
};

export default ButtonDoMore;
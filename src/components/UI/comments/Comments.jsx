import React from 'react';
import comments from './../../../assets/images/comments.svg';
import commentsHover from './../../../assets/images/comments-hover.svg';
import { useState } from 'react';

const Comments = () => {
    const [isHover, setIsHover] = useState(false)
    return (
        <div className='commentsWrap'
            style={{ cursor: "pointer" }}
            onMouseEnter={() => setIsHover(true)}
            onMouseLeave={() => setIsHover(false)
            }>
            {!isHover && (
                <img src={comments}
                    className='podcast-grid-icons-left__icon-comments'
                    alt='comments' />
            )}
            {isHover && (
                <img src={commentsHover}
                    className='podcast-grid-icons-left__icon-comments'
                    alt='comments' />
            )}
        </div>
    );
};

export default Comments;
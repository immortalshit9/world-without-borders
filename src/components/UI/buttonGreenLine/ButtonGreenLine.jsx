import React from 'react';
import classes from './buttonGreenLine.module.css';

const ButtonGreenLine = ({children}) => {
    return (
            <button type="button" className={classes.btnGreenLine}>{children}</button>
    );
};

export default ButtonGreenLine;
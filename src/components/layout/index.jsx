import React from 'react';
import Header from './../header/index.jsx';
import Footer from './../footer/index.jsx';

const index = ({ children }) => {
    return (
        <div className='layout'>
            <Header />
            {children}
            <Footer />
        </div>
    );
};

export default index;
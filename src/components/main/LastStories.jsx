import React, { useState } from 'react';
import festival from './../../assets/images/festival-in-india.png'
import humsters from './../../assets/images/humsters.png'
import canion from './../../assets/images/grand-canion.png'
import robot from './../../assets/images/claver-robot.png'
import turtle from './../../assets/images/dead-turtle.png'
import continueReading from './../../assets/images/continue-reading.svg'
import continueReadingHover from './../../assets/images/continue-reading-hover.svg'
import ButtonDoMore from '../UI/buttonDoMore/ButtonDoMore';

const LastStories = () => {
    return (
        <section className='stories'>
            <div className='stories__top'>
                <h2 className='stories__title'>
                    Последние истории
                </h2>
                <a className='stories__link' href='#'>
                    <ButtonDoMore todo={"Читать ещё"} margL={'3px'} />
                </a>
            </div>
            <div className='stories-content-wrapper'>
                <div className='stories-main-story'>
                    <a className='stories-main-story-continue-reading__link'
                        href='#'>
                        <img
                            className='stories-main-story-continue-reading__img'
                            src={continueReading}
                            alt='continue reading'
                        />
                        <img
                            className='stories-main-story-continue-reading__img-hover'
                            src={continueReadingHover}
                            alt='continue reading'
                        />
                    </a>
                    <div className='stories-main-story-img-wrapper'>
                        <img
                            className='stories-main-story__img'
                            src={festival}
                            alt='festival-in-india'
                        />
                    </div>
                    <div className='stories-main-story-tag__wrapper'>
                        <p className='stories-main-story-tag__text'>Культура</p>
                    </div>

                    <div className='stories-main-story-container'>
                        <h3 className='stories-main-story__title'>
                            Красочный фестиваль Холи в Индии во время пандеми
                        </h3>
                        <div className='stories-main-story-wrapper'>
                            <div className='stories-main-story__green-border'>
                                <p className='stories-main-story__text'>
                                    Священный праздник весны и любви раскрашивает Индию всеми цветами радуги. В марте, когда природа расцветает после зимней спячки, индийцы празднуют...
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <ul className='stories-grid-wrapper'>
                    <li className='stories-grid__item'>
                        <div className='stories-grid-story-img-wrapper'>
                            <img
                                className='stories-grid-story__img'
                                src={humsters}
                                alt='10 фактов о милых домашних грызунах'
                            />
                        </div>
                        <div className='stories-grid-story-tag__wrapper'>
                            <p className='stories-grid-story-tag__text'>Животные</p>
                        </div>
                        <h4 className='stories-grid-story__title'>10 фактов о милых домашних грызунах</h4>
                    </li>

                    <li className='stories-grid__item'>
                        <div className='stories-grid-story-img-wrapper'>
                            <img
                                className='stories-grid-story__img'
                                src={canion}
                                alt='Легкое путешествие на Гранд-Каньон за 5 дней'
                            />
                        </div>
                        <div className='stories-grid-story-tag__wrapper'>
                            <p className='stories-grid-story-tag__text'>Путешествия</p>
                        </div>
                        <h4 className='stories-grid-story__title'>Легкое путешествие на Гранд-Каньон за 5 дней</h4>
                    </li>

                    <li className='stories-grid__item'>
                        <div className='stories-grid-story-img-wrapper'>
                            <img
                                className='stories-grid-story__img'
                                src={robot}
                                alt='Умный робот для умного дома. Полезный помощник для вас'
                            />
                        </div>
                        <div className='stories-grid-story-tag__wrapper'>
                            <p className='stories-grid-story-tag__text'>Наука</p>
                        </div>
                        <h4 className='stories-grid-story__title'>Умный робот для умного дома. Полезный помощник для вас</h4>
                    </li>

                    <li className='stories-grid__item'>
                        <div className='stories-grid-story-img-wrapper'>
                            <img
                                className='stories-grid-story__img'
                                src={turtle}
                                alt='Проблемы вымирания пестрых черепах'
                            />
                        </div>
                        <div className='stories-grid-story-tag__wrapper'>
                            <p className='stories-grid-story-tag__text'>Животные</p>
                        </div>
                        <h4 className='stories-grid-story__title'>Проблемы вымирания пестрых черепах</h4>
                    </li>
                </ul>
            </div>
        </section>
    );
};

export default LastStories;
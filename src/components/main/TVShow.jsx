import React from 'react';
import africans from './../../assets/images/africans.png'
import space from './../../assets/images/space.png'
import dog from './../../assets/images/dog.png'
import facet from './../../assets/images/facet.png'
import watchMore from './../../assets/images/watch-more.svg'
import ButtonDoMore from '../UI/buttonDoMore/ButtonDoMore';

const TVShow = () => {
    return (
        <div className='tv'>
            <section className='tv-wrapper'>
                <div className='tv-top'>
                    <h2 className='tv__title'>
                        ТВ шоу
                    </h2>
                    <a className='tv__link' href='#'>
                        <ButtonDoMore todo={"Смотреть ещё"} margL={'4px'}/>
                    </a>
                </div>
                <div className='tv-grid'>
                    <div className='tv-grid__item'>
                        <a className='tv-grid__link'>
                            <img src={africans} className='tv-grid__img' alt="Африканские племена" />
                        </a>
                    </div>
                    <div className='tv-grid__item'>
                        <a className='tv-grid__link'>
                            <img src={space} className='tv-grid__img' alt="Space live" />
                        </a>
                    </div>
                    <div className='tv-grid__item'>
                        <a className='tv-grid__link'>
                            <img src={dog} className='tv-grid__img' alt="Я собачник" />

                        </a>
                    </div>
                    <div className='tv-grid__item'>
                        <a className='tv-grid__link'>
                            <img src={facet} className='tv-grid__img' alt="Грань" />
                        </a>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default TVShow;
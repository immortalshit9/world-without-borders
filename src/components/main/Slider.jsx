import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import { useRef, useState } from "react";
import "swiper/css/pagination";
import { Pagination } from "swiper";
import "swiper/css/pagination";
import hearth from './../../assets/images/hearth.svg';
import comments from './../../assets/images/comments.svg';
import forSlider from './../../assets/images/for-slider.png';

const Slider = () => {
    const pagination = {
        clickable: true,
        renderBullet: function (index, className) {
            return '<span class="' + className + '">' + (index + 1) + "</span>";
        },
    };
    return (
        <Swiper

            pagination={pagination}
            modules={[Pagination]}
            className="mySwiper"
        >
            <SwiperSlide><div className='swiper-slide'>
                <img
                    src={forSlider}
                    className='swiper-slide__img'
                    alt='slider image'
                />
                <div className='swiper-slide__tag-wrapper'>
                    <span className='swiper-slide__tag-text'>
                        Путешествия
                    </span>
                </div>
                <div className='swiper-slide-right'>
                    <h2 className='swiper-slide-right__title'>
                        Неизведанные места гор Мачу-Пикчу
                    </h2>
                    <div className='swiper-slide-right-description'>
                        <div className='swiper-slide-right-description__container'>
                            <p className='swiper-slide-right-description__text'>
                                Мачу Пикчу и горы рядом создают сакральную энергетику, сильнейшую по мощности. Не зря мистики называют их местом силы. Уайна-Пикчу — горный хребет в Перу, вокруг которого река...
                            </p>
                        </div>
                    </div>
                    <div className='swiper-slide-right-bottom-wrapper'>
                        <div className='swiper-slide-right-likes-comments-container'>
                            <div className='swiper-slide-right-likes'>
                                <img
                                    className='swiper-slide-right-likes__icon'
                                    src={hearth}
                                    alt="likes"
                                />
                                <span className='swiper-slide-right-likes__counter'>
                                    23
                                </span>
                            </div>
                            <div className='swiper-slide-right-comments'>
                                <img
                                    src={comments}
                                    className='swiper-slide-right-comments__icon'
                                    alt='comments'
                                />
                                <span className='swiper-slide-right-comments__counter'>
                                    23
                                </span>
                            </div>
                        </div>
                        <button type='button' className='swiper-slide-right__btn'>
                            Читать
                        </button>
                    </div>
                </div>
            </div></SwiperSlide>
            <SwiperSlide>Slide 2</SwiperSlide>
            <SwiperSlide>Slide 3</SwiperSlide>
            <SwiperSlide>Slide 4</SwiperSlide>
            <SwiperSlide>Slide 5</SwiperSlide>
        </Swiper>
    )
};

export default Slider;
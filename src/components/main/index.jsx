import React from 'react';
import './style.scss';
import DownloadMobile from './DownloadMobile';
import LastStories from './LastStories';
import Podcast from './Podcast';
import Slider from './Slider';
import TVShow from './TVShow';

const index = () => {
    return (
        <div className='main'>
            <h1 className='main__title'>Main page</h1>
            <Slider />
            <LastStories />
            <TVShow />
            <Podcast />
            <DownloadMobile />
        </div>
    );
};

export default index;
import React from 'react';
import apple from './../../assets/images/apple.svg'
import google from './../../assets/images/google.svg'
import banner from './../../assets/images/for-mobile.png'
import './../../assets/fonts/fonts.css'
import ButtonDownload from '../UI/buttonDownload/ButtonDownload';

const DownloadMobile = () => {
    return (
        <div className='download'>
            <section className='download-conteiner'>
                <img
                    className='download__banner-768-360'
                    src={banner}
                    alt='for mobile'
                />
                <div className='download__description'>'
                    <h2 className='download__title'>
                        Читай, смотри и слушай из любой точки мира
                    </h2>
                    <p className='download__text'>
                        Установите приложение
                        <span className='download__text-bold'>
                            &nbsp; “Мир без границ” &nbsp;
                        </span>
                        и узнавайте о невероятных историях, следите за новыми выпусками любимого шоу и прослушивайте интересные подкасты со своего смартофона даже оффлайн!
                        <p className='download__text'>Подключите &nbsp;
                            <a className='download__premium-link'>
                                премиум подписку
                            </a>
                            &nbsp; к вашему аккаунту и получите доступ к неограниченному облаку знаний.</p>
                    </p>
                    <div className='download-buttons'>
                        <ButtonDownload >
                            <img
                                className='download-buttons__icon'
                                src={apple}
                                alt="apple"
                            />
                            <div className='download-buttons-text'>
                                <span className='download-buttons-text__download'>Сачать в</span>
                                <p className='download-buttons-text__store'>AppStore</p>
                            </div>
                        </ButtonDownload >
                        <ButtonDownload >
                            <img
                                className='download-buttons__icon'
                                src={google}
                                alt="google"
                            />
                            <div className='download-buttons-text'>
                                <span className='download-buttons-text__download'>Сачать в</span>
                                <p className='download-buttons-text__store'>GooglePlay</p>
                            </div>
                        </ButtonDownload>
                    </div>
                </div>
                <img
                    className='download__banner-1920'
                    src={banner}
                    alt='for mobile'
                />
            </section>
        </div>
    );
};

export default DownloadMobile;
import React from 'react';
import hearth from './../../../../assets/images/hearth.svg';
import comments from './../../../../assets/images/comments.svg';
import Headphones from '../../../UI/headphones/Headphones';
import Hearth from '../../../UI/hearth/Hearth';
import Comments from '../../../UI/comments/Comments';
// import './../../style.scss';


const PodcastItem = ({src, alt, title}) => {
    return (
        <div className='podcast-grid__item'>
            <img
                src={src}
                className='podcast-grid__img'
                alt={alt}
            />
            <h3 className='podcast-grid__theme'>{title}</h3>
            <div className='podcast-grid-icons'>
                <div className='podcast-grid-icons-left'>
                    <Hearth />
                    <p className='podcast-grid-icons-left__likes'>23</p>
                    <Comments />
                    <p className='podcast-grid-icons-left__comments'>3</p>
                </div>
                <Headphones />
            </div>
        </div>

    );
};

export default PodcastItem;
import React, { useState } from 'react';
import data from '../../../../data/podcastData.json';
import PodcastItem from './PodcastItem';

const PodcastList = () => {
    const [dataPodcast, setDataPodcast] = useState(data.slice());

    const sixLastPodcasts = dataPodcast
        .reverse()
        .slice(0, 6)
        .map((singlePodcast) => {
            return (
                <PodcastItem
                    key={singlePodcast.id}
                    alt={singlePodcast.alt}
                    title={singlePodcast.title}
                    src={singlePodcast.src}
                />
            )
        });
    return (
        <div className='podcast-grid__wrapper'>
            {sixLastPodcasts}
        </div>
    );
}

export default PodcastList;
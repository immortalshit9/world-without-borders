import React from 'react';
import PodcastList from './smallerComponents/podcastList/PodcastList';
import ButtonDoMore from '../UI/buttonDoMore/ButtonDoMore';


const Podcast = () => {
    return (
        <div className='podcast'>
            <section className='podcast-wrapper'>
                <div className='podcast-top'>
                    <h2 className='podcast__title'>Подкасты</h2>
                    <a className='podcast__link'>
                        <ButtonDoMore todo={'Слушать ещё'} margL={'7px'}/>
                    </a>
                </div>
                <PodcastList /> 
            </section>
        </div>
    );
};

export default Podcast;
import React from 'react';
import FooterBottom from './FooterBottom';
import FooterTop from './FooterTop';
import './style.scss';

const index = () => {
    return (
        <div className='footer'>
            <FooterTop />
            <FooterBottom />
        </div>
    );
};

export default index;
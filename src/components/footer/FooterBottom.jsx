import React from 'react';
import arrowUp from './../../assets/images/arrow-up.svg';
import garpixLogo from './../../assets/images/garpix-logo.png';

const FooterBottom = () => {
    return (
        <div className='footer-bottom'>
            <div className='footer-bottom-wrapper'>
                <img
                    src={arrowUp}
                    className='footer-bottom__img'
                    alt='to top'
                />
                <ul className='footer-bottom-information-list'>
                    <li className='footer-bottom-information-list__item'>
                        <a className='footer-bottom-information-list__link'>Термины и определения</a>
                    </li>
                    <li className='footer-bottom-information-list__item'>
                        <a className='footer-bottom-information-list__link'>Политика конфиденциальности</a>
                    </li>
                    <li className='footer-bottom-information-list__item'>
                        <a className='footer-bottom-information-list__link'>Пользовательское соглашение</a>
                    </li>
                </ul>
                <div className='footer-bottom-right-wrapper'>
                    <p className='footer-bottom__right-text'>©2021 МИР БЕЗ ГРАНИЦ</p>
                    <div className='footer-bottom-right-madeby-wrapper'>
                        <p className='footer-bottom__created'>Разработано</p>
                        <img
                            src={garpixLogo}
                            className='footer-bottom__created-img'
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FooterBottom;
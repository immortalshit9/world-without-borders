import React from 'react';
import logoFooter from './../../assets/images/logo-footer.png'
import youTubeGrey from './../../assets/images/youTubeGrey.svg'
import youTubeWhite from './../../assets/images/youTubeWhite.svg'
import instagramGrey from './../../assets/images/instagramGrey.svg'
import instagramWhite from './../../assets/images/instagramWhite.svg'
import twitterGrey from './../../assets/images/twitterGrey.svg'
import twitterWhite from './../../assets/images/twitterWhite.svg'
import facebookGrey from './../../assets/images/facebookGrey.svg'
import facebookWhite from './../../assets/images/facebookWhite.svg'
import formButton1920and768 from './../../assets/images/form-button.svg'
import formButton360 from './../../assets/images/form-button360.svg'
import checkbox from './../../assets/images/checkbox.svg'

const FooterTop = () => {
    return (
        <section className='footer-top'>
            <div className='footer-top-cont'>
                <img
                    src={logoFooter}
                    alt="world without borders"
                    className='footer-top__logo'
                />
                <div className='footer-top-another-container'>
                    <div className='footer-top-list-container'>
                        <ul className='footer-top-list-left'>
                            <li className='footer-top-list-left__item'>
                                <a className='footer-top__link'>История</a>
                                <div className='hl'></div>
                            </li>
                            <li className='footer-top-list-left__item'>
                                <a className='footer-top__link'>ТВ шоу</a>
                                <div className='hl'></div>
                            </li>
                            <li className='footer-top-list-left__item'>
                                <a className='footer-top__link'>Подкасты</a>
                                <div className='hl'></div>
                            </li>
                        </ul>

                        <ul className='footer-top-list-right'>
                            <li className='footer-top-list-right__item'>
                                <a className='footer-top__link'>Животные</a>
                                <div className='hl'></div>
                            </li>
                            <li className='footer-top-list-right__item'>
                                <a className='footer-top__link'>Наука</a>
                                <div className='hl'></div>
                            </li>
                            <li className='footer-top-list-right__item'>
                                <a className='footer-top__link'>Путешествия</a>
                                <div className='hl'></div>
                            </li>
                            <li className='footer-top-list-right__item'>
                                <a className='footer-top__link'>Культура</a>
                                <div className='hl'></div>
                            </li>
                        </ul>
                    </div>
                    <div className='footer-top-social-wrapper'>
                        <ul className='footer-top-social-list'>
                            <li className='footer-top-social-list__item'>
                                <img
                                    src={youTubeGrey}
                                    className='footer-top-social-list__icon-grey'
                                    alt='YouTube'
                                />
                                <img
                                    src={youTubeWhite}
                                    className='footer-top-social-list__icon-white'
                                    alt='YouTube'
                                />
                            </li>
                            <li className='footer-top-social-list__item'>
                                <img
                                    src={instagramGrey}
                                    className='footer-top-social-list__icon-grey'
                                    alt='instagram'
                                />
                                <img
                                    src={instagramWhite}
                                    className='footer-top-social-list__icon-white'
                                    alt='instagram'
                                />
                            </li>
                            <li className='footer-top-social-list__item'>
                                <img
                                    src={twitterGrey}
                                    className='footer-top-social-list__icon-grey'
                                    alt='twitter'
                                />
                                <img
                                    src={twitterWhite}
                                    className='footer-top-social-list__icon-white'
                                    alt='twitter'
                                />
                            </li>
                            <li className='footer-top-social-list__item'>
                                <img
                                    src={facebookGrey}
                                    className='footer-top-social-list__icon-grey'
                                    alt='facebook'
                                />
                                <img
                                    src={facebookWhite}
                                    className='footer-top-social-list__icon-white'
                                    alt='facebook'
                                />
                            </li>
                        </ul>
                        <div className='footer-top-form'>
                            <h4 className='footer-top-form__title'>Подписка на рассылку</h4>
                            <input className='footer-top-form__input' placeholder='Имя' />
                            <input className='footer-top-form__input' placeholder='E-mail' />
                            <div className='footer-top-form-container'>
                                <img
                                    src={checkbox}
                                    className='footer-top-form__checkbox'
                                    alt='checkbox'
                                />
                                <p className='footer-top-form__text'>Я согласен (на) на обработку моих персональных данных</p>
                                <img
                                    src={formButton1920and768}
                                    className='footer-top-form__btn-1920-768'
                                    alt='button'
                                />
                                <img
                                    src={formButton360}
                                    className='footer-top-form__btn-360'
                                    alt='button'
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section >
    );
};

export default FooterTop;
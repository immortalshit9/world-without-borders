import React from 'react';
import HeaderTop from './HeaderTop';
import './header.scss';
import HeaderBottom from './HeaderBottom';

const index = () => {
    return (
        <div className="header">
            <HeaderTop/>
            <HeaderBottom/>
        </div>
    );
};

export default index;
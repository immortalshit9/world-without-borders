import React from 'react';

const HeaderNav = () => {
    return (
        <nav className="header-nav">
            <ul className="header-nav-list">
                <li className="header-nav-list__item">
                    <a className="header-nav-list__link" href="#">
                        Животные
                    </a>
                    <div className='vl'></div>
                </li>
                <li className="header-nav-list__item">
                    <a className="header-nav-list__link" href="#">
                        Наука
                    </a>
                    <div className='vl'></div>
                </li>
                <li className="header-nav-list__item">
                    <a className="header-nav-list__link" href="#">
                        Путешествия
                    </a>
                    <div className='vl'></div>
                </li>
                <li className="header-nav-list__item">
                    <a className="header-nav-list__link" href="#">
                        Культура
                    </a>
                    <div className='vl'></div>
                </li>
            </ul>
        </nav>
    );
};

export default HeaderNav;
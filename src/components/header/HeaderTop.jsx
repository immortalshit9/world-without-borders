import React from 'react';
import ButtonGreenLine from './../UI/buttonGreenLine/ButtonGreenLine.jsx';
import ButtonDark from './../UI/buttonDark/ButtonDark.jsx';
import logo from "./../../assets/images/logo.png";
import burger from './../../assets/images/burger.svg';


const HeaderTop = () => {
    return (
        <div className='header-top__wrapper'>
            <div className='header-top'>
                <img src={logo} className="header-top__logo" alt="logo" />
                <div className='header-top-right'>
                    <div className='header-top-right__btn'>
                        <ButtonGreenLine>Подписка</ButtonGreenLine>
                    </div>
                    <div className='header-top-right__btn'>
                        <ButtonDark>Войти</ButtonDark>
                    </div>
                </div>
                <img 
                    src={burger}
                    className='header-top__burger'
                    alt='burger'
                    />
            </div>
        </div>
    );
};

export default HeaderTop;
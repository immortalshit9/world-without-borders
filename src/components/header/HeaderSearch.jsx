import React from 'react';
import search1920 from './../../assets/images/search.svg';
import search768 from './../../assets/images/search-768.svg';

const HeaderSearch = () => {
    return (
        <div className="header-search">
            <input
                className="header-search__input"
                type="text"
                placeholder="Поиск..."
            />
            <img
                src={search1920}
                className='header-search__icon-1920'
            />
            <img
                src={search768}
                className='header-search__icon-768'
            />
        </div>
    );
};

export default HeaderSearch;
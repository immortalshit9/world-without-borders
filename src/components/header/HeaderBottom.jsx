import React from 'react';
import HeaderNav from './HeaderNav';
import HeaderSearch from './HeaderSearch';

const HeaderBottom = () => {
    return (
        <div className='header-bottom'>
            <HeaderNav/>
            <HeaderSearch/>
        </div>
    );
};

export default HeaderBottom;
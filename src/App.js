import MainPage from "./components/main/index.jsx"
import Layout from './components/layout/index.jsx'
import './styles/normalize.scss'

function App() {
  return (
    <div className="App">
      <Layout>
        <MainPage />
      </Layout>
    </div>
  );
}

export default App;
